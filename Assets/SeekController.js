﻿#pragma strict

function Start () {
	//Se ejecuta una sola vez
	Debug.Log("Esto es START");
	
}

var speed = 0.8;

function Update () {
	// Se ejecuta cada cuadro
	if(Input.GetKey("w")){
		transform.position.z += speed;
	}
	if(Input.GetKey("a")){
		transform.position.x -= speed;
	}
	if(Input.GetKey("s")){
		transform.position.z -= speed;
	}
	if(Input.GetKey("d")){
		transform.position.x += speed;
	}
	
}

